# Volley-Score

Simple tracker for Volleball score.
Created as a play project - no dependencies, no bundler, plain HTML/Javascript.
Uses local storage exclusively.
Might add some export and sharing functions later.

## package.json

This is only here to enable an npm start for local testing. 

No bundling is needed, these are plain HTML/CSS/Javascript files that will run as-is


