const VERSION = 1;
const CACHE_NAME = `volley-cache-v${VERSION}`;

self.addEventListener('activate', event => event.waitUntil(deleteStaleCaches));
self.addEventListener('fetch', event => event.respondWith(staleWhileRevalidate(event)));

async function staleWhileRevalidate(event) {
	const { request } = event;

	const cache = await caches.open(CACHE_NAME);

	const cachedResponse = await cache.match(request);
	const networkResponsePromise = fetch(request).catch(() => null);

	if (request.method === 'GET') {
		event.waitUntil(update({ cache, request, response: networkResponsePromise }));
	}

	return cachedResponse || networkResponsePromise;
}

async function update({ cache, request, response }) {
	const _response = await response;
	if (!_response) { return; }
	await cache.add(request, _response.clone());
}

async function deleteStaleCaches() {
	const cacheNames = await caches.keys();
	await Promise.all(cacheNames
		.filter(cacheName => cacheName !== CACHE_NAME)
		.map(cacheName => caches.delete(cacheName)));
}
