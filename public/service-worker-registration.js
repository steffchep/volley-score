export const registerServiceWorker = () => {
	navigator.serviceWorker
		.register('service-worker.js')
		.then(() => { console.log('ServiceWorker registered'); })
		.catch(err => { console.error('ServiceWorker registration failed', err); });
};

export const unregisterAllServiceWorkers = () => {
	if ('serviceWorker' in navigator) {
		navigator.serviceWorker.getRegistrations().then(registrations => {
			for (const registration of registrations) {
				console.log('unregistering:', registration);
				registration.unregister();
			} 
		});
	}
}

export async function deleteAllCaches() {
	const cacheNames = await caches.keys();
	await Promise.all(cacheNames
		.map(cacheName => caches.delete(cacheName)));
	console.log('caches cleared!');
	}
