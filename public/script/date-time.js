"use strict";

export const startTimeFormat = new Intl.DateTimeFormat('de-DE', { dateStyle: 'medium', timeStyle: 'short' });
export const endTimeFormat = new Intl.DateTimeFormat('de-DE', { dateStyle: undefined, timeStyle: 'short' });
