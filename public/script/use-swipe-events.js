const DURATION_THRESHOLD = 500;
const MOVE_THRESHOLD = 75;

export function useSwipeEvents(container) {
	container.addEventListener("touchstart", touchStart);
	container.addEventListener("touchend", touchEnd);
	
	// Declare variables to store the timestamp and initial touch coordinates
	const touchProps = { 
		touchStartTime: 0,
		touchEndTime: 0,
		clientX: 0,
		clientY: 0.
	};
	
	function touchStart(e) {
		// Prevent the default behavior (e.g. scrolling) of the touch event
		if (!container.hasAttribute('disabled')) {
			e.preventDefault();

			// Record the timestamp of the touch event
			touchProps.touchStartTime = Date.now();

			// Record the X and Y coordinates of the touch on the screen
			touchProps.clientY = e.touches[0].clientY;
			touchProps.clientX = e.touches[0].clientX;
		}
	}
	
	function touchEnd(e) {
		if (!container.hasAttribute('disabled')) {
			// Record the timestamp of the touch end event
			touchProps.touchEndTime = Date.now();
	
			// Call the swipe function to check if a swipe gesture occurred
			swipe(e, touchProps);
		}
	}
}

function swipe(e, { clientX, clientY, touchStartTime, touchEndTime }) {
	// Get the final X and Y coordinates of the touch
	const endClientX = e.changedTouches[0].clientX;
	const endClientY = e.changedTouches[0].clientY;
	const duration = touchEndTime - touchStartTime;

	// Check if the elapsed time between touchstart and touchend events is less than or equal to the duration threshold
	if (duration <= DURATION_THRESHOLD) {
		if (clientY - endClientY >= MOVE_THRESHOLD) {
			e.currentTarget.dispatchEvent(new Event('swipe-up'));
		} else if (endClientY - clientY >= MOVE_THRESHOLD) {
			e.currentTarget.dispatchEvent(new Event('swipe-down'));
		} else if (endClientX - clientX >= MOVE_THRESHOLD) {
			e.currentTarget.dispatchEvent(new Event('swipe-right'));
		} else if (clientX - endClientX >= MOVE_THRESHOLD) {
			e.currentTarget.dispatchEvent(new Event('swipe-left'));
		} else {
			e.currentTarget.dispatchEvent(new Event('click'));
		}
	} 
}