"use strict";

const variablePrefix = '--vs-color-';
const variableBackgroundSuffix = '-background';

const _teamColors = {
	red: {
		fg: 'var(--vs-color-team-fg-red)',
		bg: 'var(--vs-color-team-bg-red)',
	},
	orange: {
		fg: 'var(--vs-color-team-fg-orange)',
		bg: 'var(--vs-color-team-bg-orange)',
	},
	yellow: { 
		fg: 'var(--vs-color-team-fg-yellow)',
		bg: 'var(--vs-color-team-bg-yellow)',
	},
	green: {
		fg: 'var(--vs-color-team-fg-green)',
		bg: 'var(--vs-color-team-bg-green)',
	},
	blue: {
		fg: 'var(--vs-color-team-fg-blue)',
		bg: 'var(--vs-color-team-bg-blue)',
	},
	purple: {
		fg: 'var(--vs-color-team-fg-purple)',
		bg: 'var(--vs-color-team-bg-purple)',
	},
}

export const teamColors = Object.keys(_teamColors);
export const teamDefaultColors = { teamA: 'orange', teamB: 'green'};

export function setTeamColor(team, color) {
	if (!['team-a', 'team-b'].includes(team)) return;

	const body = document.querySelector('body');

	if (!teamColors.includes(color)) {
		body.style.removeProperty(`${variablePrefix}${team}`);
		body.style.removeProperty(`${variablePrefix}${team}${variableBackgroundSuffix}`);
		return;
	};

	body.style.setProperty(`${variablePrefix}${team}`, _teamColors[color].fg);
	body.style.setProperty(`${variablePrefix}${team}${variableBackgroundSuffix}`, `${_teamColors[color].bg}`);
}

export function resetTeamColors() {
	const body = document.querySelector('body');
	body.style.removeProperty(`${variablePrefix}team-a`);
	body.style.removeProperty(`${variablePrefix}team-a${variableBackgroundSuffix}`);
	body.style.removeProperty(`${variablePrefix}team-b`);
	body.style.removeProperty(`${variablePrefix}team-b${variableBackgroundSuffix}`);
}

export function setAnimationPref(enabled = true) {
	document.body.classList.toggle('reduced-motion', !enabled);
}

const darkModeMediaQuery = '(prefers-color-scheme: dark)';
const supportedColorSchemes = ['light', 'dark'];
export const useSystemColorScheme = 'system';

export function setUpColorSchemeSwitch(toggleThemeSwitch, loadColorScheme, saveColorScheme, callback) {
	const colorScheme = loadColorScheme(useSystemColorScheme);

	const mediaMatcher = window.matchMedia(darkModeMediaQuery);
	const darkModeClassName = 'has-dark-mode';

	if (supportedColorSchemes.includes(colorScheme)) {
		setColorScheme(colorScheme, darkModeClassName);
		if (colorScheme === 'dark') {
			toggleThemeSwitch.checked = true;
		}
	} 
	addMediaQueryListener(mediaMatcher, (isDark) => { 
			const currentColorScheme = loadColorScheme(useSystemColorScheme);
			if (currentColorScheme === 'system') {
				document.body.classList.toggle(darkModeClassName, isDark);
				toggleThemeSwitch.checked = isDark;
			}
		});
		
	toggleThemeSwitch.addEventListener('change', setColorSchemeFromSetting);

	function setColorSchemeFromSetting() {
		const colorScheme = toggleThemeSwitch.checked ? 'dark': 'light';
		saveColorScheme(colorScheme);
		setColorScheme(colorScheme, darkModeClassName);
		if (typeof callback === 'function') {
			callback();
		} 
	}

	return {
		setColorSchemeFromSystem: () => {
			setColorScheme(mediaMatcher.matches ? 'dark' : 'light', darkModeClassName);
		},
		setColorSchemeFromSetting,
	}
}

const registeredMediaListeners = new Set();

function addMediaQueryListener(mediaMatcher, callback) {
	const entryName = mediaMatcher.media;

	if (!registeredMediaListeners.has(entryName)) {
		mediaQueryListener();

		mediaMatcher.addEventListener('change', mediaQueryListener);
		registeredMediaListeners.add(entryName);
	}

	function mediaQueryListener() {
		if (callback) {
			callback(mediaMatcher.matches);
		}
	}
}

function setColorScheme(scheme, className, addToElement = document.body) {
	if (!supportedColorSchemes.includes(scheme)) { return; }

	if (scheme === 'light') {
		addToElement.classList.toggle(className, false);
	} else if (scheme === 'dark') {
		addToElement.classList.toggle(className, true);
	} 
}
