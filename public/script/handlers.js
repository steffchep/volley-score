"use strict";

import {  unregisterAllServiceWorkers, deleteAllCaches } from '../service-worker-registration.js';

import { 
	clearCurrentGame,
	copyToClipboard, 
	disableElements,
	enableElements,
	generateGameProtocol, 
	getConfirmation,
	hideElements,
	loadAnimationPref,
	loadCurrentGame,
	loadLog, 
	purgeStorage,
	restoreSides,
	Rules, 
	setAnimationPref,
	setupBoard,
	setupGame,
	showAlert,
	showElements,
	snapshotCurrentGame,
	storageKeyColorTheme,
	teamByKebab, 
} from './index.js';

export function addHandlers(
	{ 
		clearLogButton,
		copyButton,
		logOutput,
		endGameButton,
		nextSetButton,
		resetScoreButton, 
		settingsButton,
		settingsDialog,
		sides, 
		startGameButton, 
		startGameDialog,
		switchSidesButton, 
	}) {

	// DEBUG, remove when it's working 
	document.querySelector('#debug-unregister-service-workers')?.addEventListener('click', () => {
		getConfirmation('DEBUG: clear cache and service-worker', () => { 
			unregisterAllServiceWorkers(); 
			deleteAllCaches();
		});
	});


	let game = setupGame({ 
		sides: sides, 
		nameA: 'Team A', 
		colorA:'', 
		nameB: 'Team B',
		colorB: '',
		rules: Rules(),
	});
		
	setAnimationPref(loadAnimationPref());
	game = loadCurrentGame(game);
	let gameInProgress = game.running();

	sides.forEach((side) => {
		addPlayfieldHandler(side, game, sides);
		getCounterForSide(side).isFrozen = true;
		getTimeoutsForSide(side).disabled = true;
		if (!gameInProgress) {
			getServebuttonForSide(side).disabled = true
		}
	});
	
	setupSettingsHandlers();
	setupStartGameHandlers();
	setupPlayfieldButtons();
	setupLogButtons();

	if (gameInProgress) {
		setupBoard({ sides, ...game });
		logOutput.textContent = generateGameProtocol(game);
	} else {
		startGameDialog.open = true;
		logOutput.textContent = 'No game in progress - use "Game" to begin\n\n\n' + loadLog() ;
		hideElements(endGameButton);
		disableElements(nextSetButton, resetScoreButton, switchSidesButton);
	}

	function addPlayfieldHandler(side) {
		const counter = getCounterForSide(side);
		const timeouts = getTimeoutsForSide(side);
		const serveButton = getServebuttonForSide(side);
		counter.addEventListener('value-changed', function valueChangedListener(e) {
			const team = teamByKebab([game.teamA, game.teamB], counter.name)
			team.setCurrentScore(counter.value, game.currentSet());
			snapshotCurrentGame(game);
			if (game.setOver(game.currentSet())) {
				handleSetOver(side);
			} else if (e.detail < counter.value) {
				setServe(side);
			}
			
		});

		serveButton.addEventListener('click', (e) => setServe(side));

		timeouts.addEventListener('timeouts-updated', function timeoutsChangedListener() {
			const team = teamByKebab([game.teamA, game.teamB], counter.name)
			team.setTimeouts(timeouts.timeoutsTaken, game.currentSet());
			snapshotCurrentGame(game);
		});
	}
	
	function getCounterForSide(side) {
		return side.querySelector('vs-counter');
	}

	function getServebuttonForSide(side) {
		return side.querySelector('.change-serve-button');
	}

	function getTimeoutsForSide(side) {
		return side.querySelector('vs-timeouts');
	}

	function handleSetOver(winnerSide) {
		console.log('set over, winner is', winnerSide ? getCounterForSide(winnerSide).name : '<draw>');
		for (const side of sides) {
			getCounterForSide(side).isFrozen = true;
			getTimeoutsForSide(side).disabled = true;
			if (game.gameOver()) {
				getServebuttonForSide(side).disabled = true;
			}
		}

		logOutput.textContent = generateGameProtocol(game);

		if (game.gameOver()) {
			clearCurrentGame();
			showAlert(`Game over, winner is ${game.winner().name}`);
			return;
		}
		enableElements(nextSetButton);
	}
	
	function resetCounter(side) {
		const counter = getCounterForSide(side);
		counter.isFrozen = false;
		counter.value = 0;
		getTimeoutsForSide(side).disabled = false;
	}

	function setupLogButtons() {
		copyButton.addEventListener('click', () => copyToClipboard(logOutput.textContent));

		const keepInStorage = [ storageKeyColorTheme ];
		clearLogButton.addEventListener('click', function confirmClearLog() { 
			getConfirmation('Purge all games',
				() => {
					logOutput.textContent = '';
					purgeStorage(keepInStorage);
					window.location.reload();
				});
		});
	}
	
	function setupPlayfieldButtons() {
		resetScoreButton.addEventListener('click', function confirmResetScore() {
			getConfirmation('Reset score for current set',
				() => {
					game.teamA.setCurrentScore(0, game.currentSet());
					game.teamB.setCurrentScore(0, game.currentSet());
					sides.forEach(side => {
						resetCounter(side);
						getServebuttonForSide(side).disabled = false;
					});
					if (!game.isTimedGame()) {
						disableElements(nextSetButton);
					}
				});
		});
	
		if (game.isTimedGame()) {
			showElements(endGameButton);
		} else {
			hideElements(endGameButton);
		}

		endGameButton.addEventListener('click', function endTimedGame() {
			getConfirmation('Finish free game', 
				() => {
					game.end();
					snapshotCurrentGame(game);
					clearCurrentGame();
					showAlert(`Game over, winner is ${game.winner().name}`);
					logOutput.textContent = 'No game in progress - use "Game" to begin\n\n\n' + generateGameProtocol(game) ;
					hideElements(endGameButton);
					disableElements(nextSetButton, resetScoreButton, switchSidesButton);
					sides.forEach((side) => { 
						getCounterForSide(side).isFrozen = true;
						getTimeoutsForSide(side).disabled = true;
						getServebuttonForSide(side).disabled = true;
					});
				});
		});
	
		switchSidesButton.addEventListener('click', () => swapSides(sides));
		
		nextSetButton.addEventListener('click', function confirmNextSet() { 
			getConfirmation('Start next set',
				() => {
					startNextSet(); 
				});
		});
	}

	function setupSettingsHandlers() {
		settingsButton.addEventListener('click', (e) => { 
			e.stopPropagation();
			settingsDialog.open = !settingsDialog.open;
		});
	
		settingsDialog.addEventListener('settings-closed', () => {
			settingsButton.classList.remove('is-active');
		})
	
		settingsDialog.addEventListener('settings-opened', () => {
			settingsButton.classList.add('is-active');
		})
	
		// click event may get eaten by disabled elements on Firefox
		document.body.addEventListener('pointerdown', (e) => {
			if (e.target !== settingsDialog) {
				settingsDialog.open = false;
			}
		});
	}
	
	function setupStartGameHandlers() {
		startGameButton.addEventListener('click', () => {
			startGameDialog.open = true; 
		});

		startGameDialog.addEventListener('dialog-closed', () => {
			startGameButton.classList.remove('is-active')
		});
		startGameDialog.addEventListener('dialog-opened', () => {
			startGameButton.classList.add('is-active')
		});

		startGameDialog.addEventListener('form-cancelled', () => startGameDialog.open = false);
	
		startGameDialog.addEventListener('form-submitted', ({ detail }) => {
			game = setupGame({ 
				...detail,
				sides, 
				rules: Rules({ winsNeeded: Number(detail.winsNeeded), isTimedGame: detail.timedGame, fixedSets: detail.fixedSets }),
			});
			game.start();

			restoreSides(
				game.getSides(), 
				sides[0], 
				sides[1],
			);

			startNextSet();
				
			snapshotCurrentGame(game);
	
			logOutput.textContent = loadLog();
			gameInProgress = true;
			if (game.isTimedGame()) {
				enableElements(nextSetButton, endGameButton);
				showElements(endGameButton);
			} else {
				disableElements(nextSetButton);
				hideElements(endGameButton);
			}

			enableElements(resetScoreButton, switchSidesButton);
			sides.forEach((side) => { 
				resetCounter(side);
				getServebuttonForSide(side).disabled = false;
			});
			startGameDialog.open = false;
		});
	}

	function startNextSet() {
		game.startNextSet();
		sides.forEach(side => { 
			resetCounter(side);
			getTimeoutsForSide(side).maxTimeouts = game.maxTimeouts(game.currentSet());
			getTimeoutsForSide(side).timeoutsTaken = 0;
		});
		if (!game.isTimedGame()) {
			disableElements(nextSetButton);
		}
		logOutput.textContent = generateGameProtocol(game);
		snapshotCurrentGame(game);
	}

	function swapSides() {
		game.switchSides();
		snapshotCurrentGame(game);
		sides.forEach(side => {
			side.classList.toggle('is-left');
			side.classList.toggle('is-right');
		});
	}

	function setServe(side) {
		for (const s of sides) {
			s.classList.toggle('has-serve', false);
			const button = getServebuttonForSide(s);
			button.disabled = s === side;
			const newLabel = s.getAttribute('aria-label').replace(', is serving', '');
			s.setAttribute('aria-label', newLabel);
		}
		side.classList.toggle('has-serve', true);
		const newLabel = side.getAttribute('aria-label') + ', is serving';
		side.setAttribute('aria-label', newLabel);
	}
}
