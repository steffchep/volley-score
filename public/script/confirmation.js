"use strict";

import { getMillisFromCssProp } from "../web-components/vs-dialog.js";

const confirmationTemplate = document.createElement('template');
confirmationTemplate.innerHTML = `
	<vs-modal modal>
		<section aria-labelledby="confirmation-heading" class="dialog-content">
			<h2 id="confirmation-heading"></h2>
			<div id="help-text"></div>
			<div id="confirmation-text">Are you sure?</div>
			<div>
				<button id="confirm-yes" class="is-primary" type="submit">Yes</button>
				<button id="confirm-no" type="button">No</button>
			</div>		
		</section>
	</vs-modal>
`;

const alertTemplate = document.createElement('template');
alertTemplate.innerHTML = `
	<vs-modal modal>
		<section aria-labelledby="confirmation-heading" class="dialog-content">
			<h2 id="confirmation-heading"></h2>
			<div>
				<button id="confirm-yes" class="is-primary" type="submit">Ok</button>
			</div>		
		</section>
	</vs-modal>
`;

function Confirmation(modalElem) {
	const headingElem = modalElem.querySelector('#confirmation-heading');
	const helpTextElem = modalElem.querySelector('#confirmation-help-text');
	const yesButton = modalElem.querySelector('#confirm-yes');
	const noButton = modalElem.querySelector('#confirm-no');

	function _confirmYes(e) {
		e.stopPropagation();
		modalElem.dispatchEvent(new Event('confirm-yes'));
	}
	
	function _confirmNo(e) {
		e.stopPropagation();
		modalElem.dispatchEvent(new Event('confirm-no'));
	}
	
	function setHeading(heading) {
		headingElem.textContent = heading;
	}

	function setHelpText(heading) {
		helpTextElem.textContent = heading;
	}

	yesButton.addEventListener('click', _confirmYes);
	noButton?.addEventListener('click', _confirmNo);
	modalElem.addEventListener('keyup', (e) => {
		console.log(e.key);
		if (e.key === "Escape") {
			_confirmNo(e);
		}
	});

	return {
		setHeading,
		setHelpText,
	}
};

export function getConfirmation(text, confirmYes, confirmNo = () => {}, alertOnly = false) {
	const confirmationDiv = document.createElement('div');
	confirmationDiv.appendChild((alertOnly ? alertTemplate : confirmationTemplate).content.cloneNode(true));

	document.body.appendChild(confirmationDiv);

	const confirmationElem = confirmationDiv.querySelector('vs-modal');
	const confirmation = Confirmation(confirmationElem);

	setTimeout(() => {
		confirmation.setHeading(text);
		confirmationElem.open = true;
		// apparently you can tab out of the modal dialog after all. If focus is not set by hand, the keyup handler oft eh component won't get caught. (might need to hook into the native dialog's close to get it animating all the time)
		confirmationElem.querySelector('#confirm-yes').focus();
	}, 0);

	const transitionDuration = getMillisFromCssProp(
		getComputedStyle(confirmationElem).getPropertyValue('--modal-interactive-transition-duration')
	);

	function ensureRemove() {
		confirmationElem.open = false;
		setTimeout(() => {
			document.body.removeChild(confirmationDiv);
		}, transitionDuration + 50);
	}

	confirmationElem.addEventListener('confirm-yes', (e) => { 
		confirmationElem.open = false;
		confirmYes(e);
	});

	confirmationElem.addEventListener('confirm-no', (e) => { 
		confirmationElem.open = false;
		confirmNo(e);
	});

	confirmationElem.addEventListener('dialog-closed', (e) => {
		ensureRemove();
	});
}

export function showAlert(text, modalClosed = () => {}) {
	getConfirmation(text, modalClosed, () => {}, true);
} 