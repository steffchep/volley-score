export function disableElements(...elements) {
	elements.forEach(element => element.setAttribute('disabled', ''));
}

export function enableElements(...elements) {
	elements.forEach(element => element.removeAttribute('disabled'));
}

export function hideElements(...elements) {
	elements.forEach(element => element.style.display = 'none');
}

export function showElements(...elements) {
	elements.forEach(element => element.style.display = '');
}