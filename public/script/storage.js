"use strict";

const supportsLocalStorage = () => {
	if (!localStorage) {
		console.warn('localStorage is not supported by your browser.');
		return false;
	}
	return true;
};

const keyPrefix = 'volley-score:';
const storageKeyLog = 'log';
const storageKeyCurrentGame = 'current';
export const storageKeyAnimationPrefs = 'animationPrefs';
export const storageKeyColorTheme = 'themePreference';

export function saveLog(text) {
	saveToStorage(storageKeyLog, text);
}

export function loadLog() {
	return loadFromStorage(storageKeyLog);
}

export function snapshotCurrentGame(game) {
	saveToStorage(storageKeyCurrentGame, game.serialize());
}

export function clearCurrentGame() {
	if (!supportsLocalStorage()) { return; }
	localStorage.removeItem(keyPrefix + storageKeyCurrentGame);
}

export function loadCurrentGame(game) {
	const stored = loadFromStorage(storageKeyCurrentGame, null);
	return game.fromStorage(stored);
}

export function purgeStorage(...exceptions) {
	if (!supportsLocalStorage()) { return; }
	const savedKeys = exceptions.map((key) => [key, loadFromStorage(key)]);
	localStorage.clear();
	savedKeys.forEach((entry) => saveToStorage(entry[0], entry[1]));
}

export function loadColorScheme(defaultValue = '') {
	return loadFromStorage(storageKeyColorTheme) || defaultValue;
}

export function saveColorScheme(text) {
	return saveToStorage(storageKeyColorTheme, text);
}

export function loadAnimationPref() {
	return loadFromStorage(storageKeyAnimationPrefs, 'true') === 'true';
}

export function saveAnimationPref(value) {
	return saveToStorage(storageKeyAnimationPrefs, '' + value);
}

function saveToStorage(key, text) {
	if (!supportsLocalStorage()) { return; }

	try {
		localStorage.setItem(keyPrefix + key, text);
	} catch(e) {
		console.error('ERROR save to localStorage failed:', e);
	}
}

const loadFromStorage = (key, defaultValue = '') => {
	if (!supportsLocalStorage()) { return; }

	try {
		return localStorage.getItem(keyPrefix + key) || defaultValue;
	} catch (e) {
		console.error('ERROR in getKey - key not set', e);
		return defaultValue;
	}

};
