"use strict";

import { Team, Set } from './index.js';
import { Rules } from './rules.js';

const LEFT_SIDE = 'is-left';
const RIGHT_SIDE = 'is-right';

export function Game(a, b, r) {
	let teamA = a, teamB = b, rules = r;
	let _startTime = 0, _endTime = 0, _currentSet = -1;
	let sets = [];
	let sides = [ LEFT_SIDE, RIGHT_SIDE ];
	
	function getStartTime() { return _startTime; } 
	
	function getEndTime() { return _endTime; }

	function end() {
		_endTime = _endTime || Date.now();
		sets[_currentSet].endTime = sets[_currentSet].endTime|| Date.now();
	}
	
	function currentSet() { return _currentSet;	}

	function isTimedGame() { return rules.options.isTimedGame; }

	function start(date) {
		_startTime = date ?? Date.now();
		console.log(`Start game: ${teamA.name} vs. ${teamB.name}`);
	}

	function running() { return _currentSet >= 0 && !_endTime; }

	function maxTimeouts(set) { return rules.maxTimeouts(set); }

	function startNextSet() {
		if (!sets.length) {
			start();
		} else {
			sets[_currentSet].endTime = Date.now();
		}
		_currentSet++;
		const newSet = Set();
		sets.push(newSet);
	}

	function switchSides() { sides.reverse(); }

	function getSides() { return sides; }
	
	function winnerOfSet(set) { return rules.winnerOfSet(teamA, teamB, set); }
	
	function setOver(set) { return rules.setOver(teamA.scores[set], teamB.scores[set], set); }

	function gameOver() {
		const isOver = rules.gameOver(teamA, teamB) || _endTime;
		if (isOver) {
			end();
		}
		return isOver;
	};
	
	function setsWon() {
		const { winsA, winsB } = rules.setsWon(teamA, teamB);
		const { pointsA, pointsB } = rules.rankingPoints(teamA, teamB);
		return {
			winsA,
			winsB,
			pointsA,
			pointsB,
		}
	}

	function winner() {
		return rules.winner(teamA, teamB);
	}
	
	function serialize(stringify = true) {
		const obj = {
			_startTime, 
			_endTime, 
			_currentSet,
			sets: [...sets],
			sides: [...sides],
			rules: { ...rules.options },
			teamA: teamA.serialize(false),
			teamB: teamB.serialize(false),
		};
		return stringify ? JSON.stringify(obj) : obj;
	}

	function fromStorage(serializedString) {
		if (serializedString) {
			try {
				const serialized = JSON.parse(serializedString);
				const {
					_startTime: start, 
					_endTime: end, 
					_currentSet: cur,
					sets: s,
					sides: sd,
					rules: r,
					teamA: a,
					teamB: b,
				} = serialized;

				teamA = Team(a.name, a.alias, a.scores, a.timeouts);
				teamB = Team(b.name, b.alias, b.scores, b.timeouts);
				teamA.setColor(a.teamColor);
				teamB.setColor(b.teamColor);
				rules = Rules(r);
				sets = s;
				sides = sd;
				_startTime = start;
				_endTime = end;
				_currentSet = cur;
			} catch(e) {
				console.error('fromStorage: couldn\'t restore game', serializedString, e);
			}
		}
		return {
			currentSet,
			end, 
			fromStorage,
			gameOver,
			getStartTime, 
			getEndTime, 
			getSides,
			isTimedGame,
			maxTimeouts,
			running,
			serialize,
			setOver,
			sets, 
			setsWon,
			start, 
			startNextSet,
			switchSides,
			teamA, 
			teamB, 
			winner,
			winnerOfSet,
		};
	}

	return { 
		isTimedGame,
		currentSet,
		end, 
		fromStorage,
		gameOver,
		getStartTime, 
		getEndTime, 
		getSides,
		running,
		maxTimeouts,
		serialize,
		setOver,
		sets, 
		setsWon,
		start, 
		startNextSet,
		switchSides,
		teamA, 
		teamB, 
		winner,
		winnerOfSet,
	};
}

export function setupGame({ sides, nameA, colorA, nameB, colorB, rules }) {
	const teamA = Team(nameA, 'team-a');
	const teamB = Team(nameB, 'team-b');
	const game = Game(teamA, teamB, rules);

	teamA.setColor(colorA);
	teamB.setColor(colorB);

	setupBoard({ sides, ...game });

	return game;
}

export function setupBoard({ sides, currentSet, maxTimeouts, running, teamA, teamB, setOver, getSides }) {
	const counterA = sides[0].querySelector('vs-counter');
	const counterB = sides[1].querySelector('vs-counter');
	const timeoutsA = sides[0].querySelector('vs-timeouts');
	const timeoutsB = sides[1].querySelector('vs-timeouts');

	sides[0].setAttribute('aria-label', teamA.name);
	sides[0].querySelector('.team-name').textContent = teamA.name;
	counterA.name = teamA.kebap;
	sides[1].setAttribute('aria-label', teamB.name);
	sides[1].querySelector('.team-name').textContent = teamB.name;
	counterB.name = teamB.kebap;

	if (running()) {
		const set = currentSet();
		const shouldDisable = setOver(set);
		const timeouts = maxTimeouts(set);
		
		counterA.value = teamA.scores[set];
		counterB.value = teamB.scores[set];

		counterA.isFrozen = shouldDisable;
		counterB.isFrozen = shouldDisable;

		timeoutsA.maxTimeouts = timeouts;
		timeoutsB.maxTimeouts = timeouts;

		timeoutsA.timeoutsTaken = teamA.timeouts[set];
		timeoutsB.timeoutsTaken = teamB.timeouts[set];

		timeoutsA.disabled = shouldDisable;
		timeoutsB.disabled = shouldDisable;

		restoreSides(getSides(), sides[0], sides[1]);
	}
}
export function restoreSides(sidesCss, first, second) {
	first.classList.toggle(sidesCss[0], true);
	first.classList.toggle(sidesCss[1], false);

	second.classList.toggle(sidesCss[1], true);
	second.classList.toggle(sidesCss[0], false);
}
