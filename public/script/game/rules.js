"use strict";

// rules https://www.volleyballer.de/regeln/regeln-db-index.php

export const officialRulesOptions = {
	winsNeeded: 3, 
	pointsForSet: 25, 
	pointsForTiebreak: 15,
	timeoutsForSet: 2,
	timeoutsForTiebreak: 2,
	isTimedGame: false,
	fixedSets: false,
}

export function Rules(opts = officialRulesOptions ) {
	const rulesOptions = { ...officialRulesOptions, ...opts};
	const { 
		winsNeeded, 
		pointsForSet, 
		pointsForTiebreak,
		timeoutsForSet,
		timeoutsForTiebreak, 
		isTimedGame,
		fixedSets,
	} = rulesOptions;
	const maxSets = winsNeeded * 2 - 1;

	function maxPoints(setAsIndex) {
		const set = setAsIndex + 1;
		return set === maxSets ? pointsForTiebreak : pointsForSet;
	}

	function maxTimeouts(setAsIndex) {
		return setAsIndex + 1 == maxSets ? timeoutsForTiebreak : timeoutsForSet;
	}

	function setOver(scoreA, scoreB, set) {
		if (isTimedGame) { return false; }
		const points = maxPoints(set);
		if (Math.abs(scoreA - scoreB) < 2) return false;
		if (scoreA < points && scoreB < points) return false;
		return true;
	}

	function winnerOfSet(teamA, teamB, set) {
		const scoreA = teamA.scores[set] || 0;
		const scoreB = teamB.scores[set] || 0;
		if (!setOver(scoreA, scoreB, set) && !isTimedGame) return {};
		if (scoreA == scoreB) { return { name: 'undecided', alias: 'undecided' }; }
		return scoreA > scoreB ? teamA : teamB;
	}

	function gameOver(teamA, teamB) {
		if (isTimedGame) { return false; }
		const { winsA, winsB } = setsWon(teamA, teamB);
		if ( fixedSets ) {
			return winsA + winsB >= winsNeeded;
		}
		return winsA === winsNeeded || winsB === winsNeeded;
	};

	function setsWon(teamA, teamB) {
		const winsA = teamA.scores.filter((score, index) => (score || 0) > (teamB.scores[index] || 0)).length;
		const winsB = teamB.scores.filter((score, index) => (score || 0) > (teamA.scores[index] || 0)).length;
		return { winsA, winsB };
	}
	function winner(teamA, teamB) {
		if (!gameOver(teamA, teamB) && !isTimedGame) return {};
		const { winsA, winsB } = setsWon(teamA, teamB);
		if ((fixedSets || isTimedGame) && winsA == winsB) {
			return { name: 'undecided', alias: 'undecided' };
		}
		return winsA > winsB ? teamA : teamB;
	}

	function rankingPoints(teamA, teamB) {
		if (!gameOver(teamA, teamB) || winsNeeded !== 3 || isTimedGame) return {};
		const { winsA, winsB } = setsWon(teamA, teamB);
		
		return {
			pointsA: rankingPointsForTeam(winsA, winsB),
			pointsB: rankingPointsForTeam(winsB, winsA),
		}
	}

	return {
		options: { ...rulesOptions, maxSets },
		gameOver,
		rankingPoints,
		maxPoints,
		maxTimeouts,
		setOver,
		setsWon,
		winner,
		winnerOfSet,
	}
}

function rankingPointsForTeam(ownScore, otherScore) {
	if (ownScore === 3) {
		return otherScore < 2 ? 3 : 2;
	}
	return ownScore < 2 ? 0 : 1;
}