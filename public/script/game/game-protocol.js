"use strict";

import { startTimeFormat, endTimeFormat, loadLog, saveLog } from '../index.js';

const blankLine = '';
const separator = '--------------------------------------------------------------';
const lineSeparator = '\r\n';

export const generateGameProtocol = (game) => {

	const oldGames = loadLog();

	const currentGame = [
		blankLine, blankLine,
		generateGameInfo(game),
		generateSetResults(game),
		generateGameResults(game),
		blankLine, separator, blankLine,
	].join(lineSeparator);

	if (game.gameOver()) {
		saveLog(currentGame + oldGames);
	}

	return currentGame + oldGames;
};

function generateGameInfo(game) {
	const lines = [];
	const { teamA, teamB } = game;

	lines.push(`${teamA.name.toUpperCase()} vs. ${teamB.name.toUpperCase()}`);
	
	if (!game.gameOver()) {
		lines.push(`Start: ${startTimeFormat.format(game.getStartTime())}`)
	} else {
		const duration = getFormattedDuration(game.getStartTime(), game.getEndTime());
		lines.push(`Start: ${startTimeFormat.format(game.getStartTime())}, end: ${endTimeFormat.format(game.getEndTime())} - total duration ${duration}`);
	}
	
	lines.push(blankLine);

	return asPlainText(lines);
}

function generateSetResults(game) {
	const lines = [];
	const { teamA, teamB, sets, isTimedGame, gameOver } = game;

	const setsPlayed = sets.length;

	for (let set = 0; set < setsPlayed; set++) {
		const scoreA = teamA.scores[set] || 0;
		const scoreB = teamB.scores[set] || 0;
		if (scoreA > 0 || scoreB > 0 || 
			(isTimedGame() && (set < (setsPlayed - 1) || gameOver()))) {
			const duration = getFormattedDuration(sets[set].startTime, sets[set].endTime || Date.now());
			const timeouts = `${teamA.timeouts[set] || 0}:${teamB.timeouts[set] || 0}`
			lines.push(
				`Set ${set + 1} ${leadingZeroes(scoreA)}:${leadingZeroes(scoreB)}, duration: ${duration}, timeouts: ${timeouts} - Winner: ${game.winnerOfSet(set).name}`
			);
		}
	}

	return asPlainText(lines);
}

function generateGameResults(game) {
	const lines = [];
	const { teamA, teamB } = game;

	if (game.gameOver()) {
		const { winsA, winsB, pointsA, pointsB } = game.setsWon();
		const totalA = teamA.scores.reduce((all, i) => all + i, 0);
		const totalB = teamB.scores.reduce((all, i) => all + i, 0);
		const rankingPoints = pointsA || pointsB ? `, Ranking points: ${pointsA}:${pointsB}`: '';
		lines.push('');
		lines.push(`Result: ${winsA}:${winsB}, Total Points: ${totalA}:${totalB}${rankingPoints}`);
		lines.push(`Match-Winner: ${game.winner()?.name}`);
	}

	return asPlainText(lines);
}

function asPlainText(array) {
	return array.join(lineSeparator);
}

function leadingZeroes(number) {
	return(number.toString().padStart(2, ' '));
}

function getFormattedDuration(startTime, endTime) {
	const delta = (endTime - startTime) / 1000 / 60;
	
	return parseInt(delta) + 'm';
}