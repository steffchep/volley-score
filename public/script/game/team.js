"use strict";

import { setTeamColor, toKebapCase } from "../index.js";

// Todo, this might work better as a class
export function Team(n, a, s, t) {
	const name = n, alias = a, kebap = toKebapCase(n);

	let scores = s || [0, 0, 0, 0, 0];
	let timeouts = t || [0, 0, 0, 0, 0];

	let teamColor = '';

	function setColor(color) {
		teamColor = color;
		setTeamColor(alias, color);
	}

	function setCurrentScore(score, set) {
		scores[set] = score;
	}

	function setTimeouts(timeoutsTaken, set) {
		timeouts[set] = timeoutsTaken;
	}

	function serialize(stringify = true) {
		const obj = { name, alias, teamColor, scores: [ ...scores ], timeouts: [...timeouts] };
		return stringify ? JSON.stringify(obj) : obj;
	}

	return { name, alias, kebap, teamColor, scores, setColor, setCurrentScore, setTimeouts, serialize, timeouts };
}

export const teamByKebab = (teams, kebap) => teams.find(team => team.kebap === kebap);

