import { toKebapCase } from "./utils.js";

test('makes kebap-case from sentence', () => {
	expect(toKebapCase('iamhere')).toBe('iamhere');
	expect(toKebapCase('I am here')).toBe('i-am-here');
})