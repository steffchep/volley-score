"use strict";

export function toKebapCase(string) {
	return string.toLowerCase().replaceAll(/[\s\/\,\.']/g, '-');
}
