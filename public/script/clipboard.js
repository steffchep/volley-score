"use strict";

export const copyToClipboard = async (content) => {
	if (navigator.clipboard) {
		await navigator.clipboard.writeText(content);
	} else {
		console.log('clipboard not supported');
	}
};

export const clipboardSupported = () => !!navigator.clipboard;