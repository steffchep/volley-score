"use strict";

export default /*css*/`
:host {
	box-sizing: border-box;
	display: inline-block;
	border-radius: var(--vs-spacing-radius);
}

:host([disabled]) {
	opacity: 0.5;
	filter: saturate(30%);
}

:host([disabled]) vs-checkbox[disabled] {
	opacity: 1;
}

fieldset {
	border-radius: inherit;
	border: 1px solid var(--vs-color-main-border);
	font-size: smaller;
	display: inline-flex;
	align-items: center;
	justify-content: center;
	margin: 0;
	line-height: 1.5em;
	padding: var(--vs-timeouts-fieldset-padding, var(--vs-spacing-1x) var(--vs-spacing-2x) var(--vs-spacing-2x) var(--vs-spacing-2x));
}

fieldset span {
	display: inline-flex;
	gap: var(--vs-timeouts-fieldset-gap, var(--vs-spacing-2x));
}

vs-checkbox {
	font-size: var(--vs-timeouts-font-size, 2em);
}
`;