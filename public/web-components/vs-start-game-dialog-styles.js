"use strict";

export default /*css*/`

@import '/styles/styles.css';
:host {
	line-height: 1.5em;
	color: inherit;
	box-sizing: border-box;
	--modal-content-width: var(--vs-start-game-dialog-width);
}
label {
	display: grid;
	width: 100%;
}

label .is-disabled,
label [disabled] {
	opacity: 0.5;
	box-shadow: none;
}

form .fields {
	display: grid;
	gap: var(--vs-spacing-2x);
	grid-template-columns: 1fr 1fr;
	margin-bottom: var(--vs-spacing-2x);
}
select {
	padding: var(--vs-spacing-2x);
	border: 1px solid var(--vs-color-main-border);
	border-radius: var(--vs-spacing-radius);
	font-size: inherit;
	line-height: inherit;
	background: var(--vs-color-input-background);
}
.buttons {
	text-align: center;
}
.legend {
	font-size: smaller;
	padding: var(--vs-spacing-2x) 0 var(--vs-spacing-4x);
}
.buttons button {
	margin-top: var(--vs-spacing-4x);
}
.switches {
	margin-top: var(--vs-spacing-2x);
	display: flex;
	align-items: stretch;
	gap: var(--vs-spacing-2x);
}
.switches vs-switch {
	flex-basis: 50%;
	flex-shrink: 0;
	flex-grow: 1;
}
@media screen and (max-width: 350px) {
	form .fields {
		grid-template-columns: 1fr;
	}
}
@media (hover: hover) {
	select:hover {
		cursor: pointer;
	}
}
`;