"use strict";

// stupid workaround, requires page reload reload when color scheme or value of property changes
// why: https://stackoverflow.com/questions/58818299/css-variables-not-working-in-dialogbackdrop
const colorModalVeil = getComputedStyle(document.body).getPropertyValue('--modal-color-veil');

export default /*css*/`

:host {
	box-sizing: border-box;
}

dialog {
	border-radius: var(--modal-spacing-radius, 0);
	transform-origin: var(--modal-interactive-transform-origin, top);
	background: var(--vs-color-main-background);
	border: none;
	padding: 0;
	box-shadow: var(--modal-box-shadow);
}

::backdrop {
	background-color: ${colorModalVeil};
}

.modal-content {
	box-sizing: border-box;
	background: var(--modal-color-content-background, #fff);
	color: var(--modal-color-content-foreground);
	width: var(--modal-content-width, clamp(300px, 30vw, 1000px));
	max-height: var(--modal-content-max-height, 90dvh);
	overflow-y: auto;
	padding: var(--vs-spacing-4x);	
}

@media screen and (max-width: 510px) {
	:host {
		--modal-interactive-transform-visible: var(--modal-interactive-transform-visible-mobile);
		--modal-interactive-transform-invisible: var(--modal-interactive-transform-invisible-mobile);
	}
	dialog {
		--modal-interactive-transform-origin: var(--modal-interactive-transform-origin-mobile, bottom);
		margin: var(--modal-interactive-margin-mobile, auto auto 0 auto);
		max-width: 96%;
		width: 96%;
	}
	.modal-content {
		width: 100%;
	}
}
`;