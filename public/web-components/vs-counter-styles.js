"use strict";

export default /*css*/`
:host {
	container-type: inline-size;
	container-name: host;
	box-sizing: border-box;
	display: block;
}

.counter-container {
	display: grid;
	grid-template-rows: 1fr 2rem;
	height: 1.15em;
	font-size: var(--vs-counter-size, clamp(3rem, 70cqw, 70cqw));
	letter-spacing: var(--vs-counter-letter-spacing, 0);
	line-height: 1em;
	color: inherit;
}

button {
	padding: 0;
	position: relative;
	border: none;
	background: rgba(0, 0, 0, 0);
	font-weight: bold;
	letter-spacing: inherit;
	overflow: hidden;
	cursor: pointer;
}
button:focus-visible {
	outline: 2px solid var(--vs-color-interactive-outline);
	outline-offset: -4px;
}
button:focus:not([disabled]) {
	background: var(--vs-color-interactive-background-hover-subtle);
}
button:active:not([disabled]) {
	background: var(--vs-color-interactive-background-hover);
	outline: 2px solid var(--vs-color-interactive-background);
}
button.vs-counter-number {
	color: inherit;
	font-size: inherit;
	line-height: inherit;
	font-weight: normal;
	max-height: 1.1em;
	overflow: hidden;
}
.vs-counter-slider {
	visibility: hidden;
	display: flex;
	gap: 0;
	flex-direction: column;
	transform: translateY(0);
}
.vs-counter-current {
	visibility: visible;
	position: absolute;
	inset: 0.002em 0 0 0;
}
button[disabled] {
	opacity: 0.8;
	filter: saturate(50%);
	cursor: default;
}
button.vs-counter-minus svg {
	height: 100%;
	width: 100%;
	fill: var(--vs-color-interactive-background-hover);
}
button.vs-counter-minus svg {
	transform: rotate(180deg);
}
button .is-overlaid {
	position: absolute;
	inset: -1px;
	font-weight: bold;
	font-size: 1.4em;
}

@container (max-width: 250px) {
	.counter-container {
		height: 1.3em;
	}
}

@container (max-width: 150px) {
	.counter-container {
		height: 1.5em;
	}
}

@media (hover: hover) {
	button:hover:not([disabled]) {
		opacity: 0.8;
	}
	button.vs-counter-number:hover:not([disabled]) {
		background: var(--vs-color-interactive-background-hover-subtle);
		opacity: 1;
	}
}
`;