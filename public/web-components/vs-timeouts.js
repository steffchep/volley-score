"use strict";

import styles from './vs-timeouts-styles.js';
const template = document.createElement('template');
template.innerHTML = `
<style>
</style>
<fieldset class="is-timeouts" style="display: none">
	<legend>Timeouts</legend>
	<span id="timeouts-container" aria-live="polite">
	</span>
</fieldset>
`;

const MAX_TIMEOUTS_PER_SET = 3;
const MIN_TIMEOUTS_PER_SET = 0;

export class VsTimeouts extends HTMLElement {
	_maxTimeouts = 0;
	_checkboxes = [];

	constructor() {
		super();
		this.attachShadow({ mode: 'open' });
		this.shadowRoot.appendChild(template.content.cloneNode(true));
		this.shadowRoot.querySelector('style').innerHTML = styles;
		this._fieldset = this.shadowRoot.querySelector('fieldset');
		this._timeoutsContainer = this.shadowRoot.querySelector('#timeouts-container');
		this.makeCheckboxes();
	}

	get disabled() {
		return this.hasAttribute('disabled');
	}

	set disabled(value) {
		if (value) {
			this.setAttribute('disabled', '');
		} else {
			this.removeAttribute('disabled');
		}
		this.updateDisabled();
	}

	get maxTimeouts() {
		return this._maxTimeouts;
	}

	set maxTimeouts(value) {
		if (Number(value) < MIN_TIMEOUTS_PER_SET || value + 0 > MAX_TIMEOUTS_PER_SET) { return; }
		this._maxTimeouts = Number(value);
		this.makeCheckboxes();
	}

	get timeoutsTaken() {
		return this._checkboxes.filter((checkbox) => checkbox.checked).length
	}

	set timeoutsTaken(value) {
		this._checkboxes.forEach((checkbox, index) => {
			checkbox.checked = index < value;
		});
		this.updateDisabled();
	}

	makeCheckboxes() {
		this._fieldset.style.display = 'none';
		this.removeEventListeners();
		this._timeoutsContainer.innerHTML = '';
		this._checkboxes.splice(0, this._checkboxes.length);

		if (this._maxTimeouts == 0) { return; }

		this._checkboxes = Array.from(new Array(this._maxTimeouts), (_, index) => { 
			const currentCount = index + 1
			const item = document.createElement('vs-checkbox');
			item.label = String(currentCount);
			item.disabled = index > 0;
			this._timeoutsContainer.appendChild(item);
			return item;
		});

		this.addEventListeners();
		
		this._fieldset.style.display = '';
	}

	addEventListeners() {
		if (this._checkboxes.length > 0) { 
			this._checkboxes.forEach((checkbox) => checkbox.addEventListener('change', this.updateDisabled));
			this._checkboxes.forEach((checkbox) => checkbox.addEventListener('change', this.dispatchChangedEvent));
		}
	}

	removeEventListeners() {
		if (this._checkboxes.length > 0) { 
			this._checkboxes.forEach((checkbox) => checkbox.removeEventListener('change', this.updateDisabled));
			this._checkboxes.forEach((checkbox) => checkbox.removeEventListener('change', this.dispatchChangedEvent));
		}
	}

	updateDisabled = (() => {
		for (var i = this._checkboxes.length - 1; i >= 0; i-- ) {
			const current = this._checkboxes[i];
			if (this.disabled) {
				current.disabled = true;
				continue;
			}
			if (i == 0) { 
				current.disabled = false;
				break;
			}
			const previous = this._checkboxes[i - 1];
			const next = i == this._checkboxes.length - 1 ? null : this._checkboxes[i + 1];
			current.disabled = !current.checked && !previous.checked && (!next || !next.checked);
		}
	}).bind(this);

	dispatchChangedEvent = (() => {
		this.dispatchEvent(new Event('timeouts-updated'));
	}).bind(this);
}

