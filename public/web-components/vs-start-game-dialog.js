"use strict";

import styles from './vs-start-game-dialog-styles.js';
import { teamColors, teamDefaultColors } from '../script/style-helpers.js';
const template = document.createElement('template');
template.innerHTML = /* html */`
<style>
</style>
<vs-modal id="start-game-modal" modal>
	<section aria-labelledby="start-new-game-heading">
		<h2 id="start-new-game-heading">Start New Game</h2>
		<form>
			<div class="fields">
				<label>
					<div>Team A</div>
					<input type="text" id="nameA">
				</label>
				<label>
					<div>Team B</div>
					<input type="text" id="nameB">
				</label>
				<label>
					<div>Team A Color</div>
					<select id="colorA">
						<option>test</option>
					</select>
				</label>
				<label>
					<div>Team B Color</div>
					<select id="colorB">
						<option>test</option>
					</select>
				</label>
				<label>
					<div id="number-of-sets"></div>
					<input type="text" id="winsNeeded">
				</label>
				<div><!-- empty grid cell --></div>
			</div>
			<div class="switches">
				<vs-switch id="timedGame" label="Free play"></vs-switch>
				<vs-switch id="fixedSets" label="Fixed # of sets"></vs-switch>
			</div>
			<div class="legend">
			<em>Free play</em> means you manually end each set and the game, 
			good for timed games or scoring in other sports games
			</div>
			<div class="buttons">
				<button id="submit" type="submit" class="is-primary">Start New Game</button>
				<button id="cancel" type="button">Cancel</button>
			</div>
		</form>
	</section>
</vs-modal>
`;

const defaultSettings = {
	nameA: 'Team A',
	colorA: teamDefaultColors.teamA,
	nameB: 'Team B',
	colorB: teamDefaultColors.teamB,
	winsNeeded: 3,
	timedGame: false,
	fixedSets: false,
};

export class VsStartGameDialog extends HTMLElement {
	constructor() {
		super();

		this.textFixedSets = 'Number of Sets';
		this.textWinsRequired = 'Wins Required';

		this.attachShadow({ mode: 'open' });
		this.shadowRoot.appendChild(template.content.cloneNode(true));
		this.shadowRoot.querySelector('style').innerHTML = styles;

		this.form = this.shadowRoot.querySelector('form');

		for (const prop of Object.keys(defaultSettings)) {
			this[prop] = this.shadowRoot.getElementById(prop);
		} 
		this.cancelButton = this.shadowRoot.getElementById('cancel');

		this.shadowRoot.getElementById('nameA').focus();

		this._addColorOptions();
		this.settings = { ...defaultSettings };
		this.modalElem = this.shadowRoot.getElementById('start-game-modal');

		this.winsNeededLabel = this.shadowRoot.getElementById('number-of-sets');
		this.winsNeededLabel.innerText = this.settings.fixedSets ? this.textFixedSets : this.textWinsRequired;

		this._handlerMap = {};
		this.form.addEventListener('submit', this.submit);
		this.form.addEventListener('keydown', this.cancelViaEscape);
		this.cancelButton.addEventListener('click', this.cancel);
		
		for (const prop of Object.keys(defaultSettings)) {
			this._handlerMap[prop] = (event) => {
				this.settings = { [prop]: this[prop].tagName == 'VS-SWITCH' ? event.currentTarget.checked : event.currentTarget.value };
			 };
			 this[prop].addEventListener('change', this._handlerMap[prop]);
		} 

		this.fixedSets.addEventListener('change', (event) => {
			this.winsNeededLabel.innerText = event.currentTarget.checked ? this.textFixedSets : this.textWinsRequired;
		});
		this.timedGame.addEventListener('change', (event) => {
			this.winsNeeded.disabled = event.currentTarget.checked;
			this.winsNeededLabel.classList.toggle('is-disabled', event.currentTarget.checked);
		});
	}

	connectedCallback() {
		this.open = this.hasAttribute(open);
	}

	get settings() {
		return this._settings || {};
	}

	set settings(settings) {
		if (!this._settings) this._settings = {};
		this._settings = { ...this._settings, ...settings };
		this._mapSettingsToInputs();
	}

	set open(value) {
		if (value) {
			this.setAttribute('open', '');
		} else {
			this.removeAttribute('open');
		}
		if (value === this.modalElem.open) return;
		this.modalElem.open = value;
		if (value) this.focusFirst();
	}

	get open() {
		return this.modalElem.open;	
	}

	_mapSettingsToInputs() {
		for (const prop of Object.keys(defaultSettings)) {
			if (this[prop].tagName == 'VS-SWITCH') {
				setTimeout(() => {
					// give component time to init
					this[prop].checked = this._settings[prop];
				}, 0)
			} else {
				this[prop].value = this._settings[prop];
			}
		} 
	}

	submit = ((event) => {
		event.preventDefault();
		this.dispatchEvent(new CustomEvent('form-submitted', { 
			detail: this._settings,			
		}));
	}).bind(this);

	cancel = (() => {
		this.dispatchEvent(new Event('form-cancelled'));
	}).bind(this);

	cancelViaEscape = ((event) => {
		if (event.key === 'Escape') {
			this.cancel()
		}
	}).bind(this);


	focusFirst = (() => {
		this[Object.keys(defaultSettings)[0]]?.focus();
	}).bind(this);

	_addColorOptions() {
		let options = '<option></option>'
		for (const color of teamColors) {
			options += `<option>${color}</option>`
		}
		this.colorA.innerHTML = options;
		this.colorB.innerHTML = options;
	}

}
