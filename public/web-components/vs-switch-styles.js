"use strict";

export default /* css */`
	:host {
		box-sizing: border-box;
		display: inline-grid;
		align-items: center;
		position: relative;
		grid-template-columns: 3.2em 1fr;
		padding: var(--vs-spacing-1x);
		font-size: var(--switch-button-size, 1rem);
		border-radius: var(--vs-spacing-radius, 99999px);
	}
	/* :host(:not([disabled]):has(:focus-visible)) -> :has does not work with shadow-dom */
	:host(:not([disabled]):focus-within) {
		outline: 2px solid var(--vs-color-interactive-outline, initial);
		outline-offset: 2px;
		box-shadow: 0px 0px 0px 2px var(--vs-color-main-background);
	}
	:host([disabled]) {
		opacity: 0.5;
		filter: saturate(30%);
	}
	label {
		padding-left: var(--vs-spacing-2x);
		cursor: inherit;
	}
	input {
		position: absolute;
		top: 0; 
		left: 0;
		opacity: 0;
	}
	.switch-window {
		width: 3.3em;
		height: 1.5em;
		overflow: hidden;
		color: var(--vs-color-interactive, blue);
		background-color: var(--vs-color-main-background, white);
		border: 1px solid var(--vs-color-main-border);
		border-radius: var(--vs-spacing-radius, 99999px);
		box-shadow: var(--vs-interactive-shadow);
	}
	input + .switch-label {
		width: max-content;
		display: grid;
		place-items: center;
		grid-template-columns: 2em 1.1em 2em;
		transition: transform var(--modal-interactive-transition-duration, 400ms) var(--modal-interactive-transition-function, ease-in);
	}
	.switch-label .thumb {
		border: 2px solid var(--vs-color-interactive, blue);
		border-radius: var(--vs-spacing-radius, 99999px);
		background-color: var(--vs-color-main-background, blue);
		width: 1.1em;
		height: 1.1em;
		transition: background-color var(--modal-interactive-transition-duration, 400ms) var(--modal-interactive-transition-function, ease-in);
	}

	input[checked] + .switch-label .thumb {
		background-color: var(--vs-color-interactive, blue);
		border-color: var(--vs-color-interactive, blue);
	}

	input[checked] + .switch-label {
		transform: translateX(-1.8em);
	}
	
	@media (hover: hover) {
		:host(:not([disabled]):hover) {
			cursor: pointer;
			background: var(--vs-color-interactive-background-hover);
		}
	}
`;
