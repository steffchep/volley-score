"use strict";

import styles from './vs-switch-styles.js';

const template = document.createElement('template');
template.innerHTML = /* html */`
<style>
</style>
<div class="switch-window">
	<input
		id="the-checkbox" 
		role="switch"
		type="checkbox">
	<div class="switch-label" aria-hidden="true">
		<div class="track is-off">off</div>
		<div class="thumb"></div>
		<div class="track is-on">on</div>
	</div>
</div>
<label for="the-checkbox" id="the-label"></label>
`;

export class VsSwitch extends HTMLElement {
	constructor() {
		super();
		const shadowRoot = this.attachShadow({ mode: 'open' });
		shadowRoot.appendChild(template.content.cloneNode(true));
		this.shadowRoot.querySelector('style').innerHTML = styles;
		this._checkboxElement = shadowRoot.querySelector('#the-checkbox');
		this._labelElement = shadowRoot.querySelector('#the-label');
		this._switchElement = shadowRoot.querySelector('.switch-label');
		
		this._checkboxElement.addEventListener('change', this._toggle);
		this._switchElement.addEventListener('click', () => { 
			if (!this.disabled) {
				this._checkboxElement.focus();
				this._toggle();
			}
		})
		
		// workarounds for Focus trap if vs-modal used
		if (typeof HTMLDialogElement !== 'function') {
			this.oldModalWorkaround();
		}
	}

	connectedCallback() {
		this.checked = this.hasAttribute('checked');
		this.disabled = this.hasAttribute('disabled');
		this.label = this.getAttribute('label');
	}

	set checked(value) {
		if (value) {
			this._checkboxElement.setAttribute('checked', '');
			this.setAttribute('checked', '');
		} else {
			this._checkboxElement.removeAttribute('checked');
			this.removeAttribute('checked');
		}
		this._checkboxElement.setAttribute('aria-checked', String(value));
	}

	get checked() {
		return this.hasAttribute('checked');
	}

	set disabled(value) {
		if (value) {
			this.setAttribute('disabled', '');
		} else {
			this.removeAttribute('disabled');
		}
		this.setAttribute('aria-disabled', String(value));
		this._checkboxElement.disabled = value;
	}

	get disabled() {
		return this.hasAttribute('disabled');
	}

	get label() {
		return this._labelElement.textContent;
	}

	set label(value) {
		this.setAttribute('label', value);
		this._labelElement.textContent = value;
	}

	_toggle = (() => {
		if (this.disabled) { return; }
		this.checked = !this.checked;
		this.dispatchEvent(new Event('change'));
	}).bind(this);

	oldModalWorkaround() {
		console.warn('vs-switch in legacy mode');
		this.addEventListener('keydown', (e) => {
			if (e.target == e.currentTarget && e.code === 'Space') {
				this._toggle();
			}
		});
		this._checkboxElement.addEventListener('keydown', (e) => {
			if (e.code === 'Space') {
				// part of focus trap workaround - if it bubbles, we toggle twice unless we stop propagation
				e.stopPropagation();
			}
		});
	}
}

