'use strict';

import styles from './vs-checkbox-styles.js';
const template = document.createElement('template');
template.innerHTML = /* html */`
<style>
</style>
<div class="vs-checkbox">
	<label>
		<span id="the-label"></span>
		<input id="the-input" type="checkbox">
	</label>
</div>
`;

export class VsCheckbox extends HTMLElement {
	constructor() {
		super();
		const shadowRoot = this.attachShadow({ mode: 'open' });
		shadowRoot.appendChild(template.content.cloneNode(true));
		this.shadowRoot.querySelector('style').innerHTML = styles;
		this._labelElement = shadowRoot.querySelector('#the-label');
		this._inputElement = shadowRoot.querySelector('#the-input')
		this._inputElement.addEventListener('change', (e) => this.checked = e.currentTarget.checked);
	}

	connectedCallback() {
		this._labelElement.innerText = this.getAttribute('label');
		if (this.hasAttribute('checked')) {
			this._inputElement.checked = true;
		}
		if (this.hasAttribute('disabled')) {
			this._inputElement.disabled = true;
		}
	}

	set label(value) {
		this._labelElement.innerText = value;
		this.setAttribute('label', value);
	}

	get label() {
		return this._labelElement.innerText;
	}

	set checked(value) {
		if (this.checked == value) { return; };
		this._inputElement.checked = !!value;
		if (!!value) {
			this.setAttribute('checked', '');
		} else {
			this.removeAttribute('checked')
		}
		this.dispatchEvent(new Event('change'));
	}

	get checked() {
		return this.hasAttribute('checked');
	}

	set disabled(value) {
		this._inputElement.disabled = !!value;
		if (!!value) {
			this.setAttribute('disabled', '');
		} else {
			this.removeAttribute('disabled')
		}
	}

	get disabled() {
		return this._inputElement.disabled;
	}
}

