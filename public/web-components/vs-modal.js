"use strict";

import styles from './vs-modal-styles.js';

/*
CSS-VARIABLES (and defaults)
--modal-color-content-background: #fff;
--modal-color-content-foreground: initial;
--modal-color-veil: rgba(0, 0, 0, 0.25);

--modal-content-max-height: 90dvh;
--modal-content-width: clamp(300px, 80vw, 1000px);

--modal-interactive-transition-function: ease-in;
--modal-interactive-transition-duration: 500ms;
--modal-interactive-transform-origin: top left;
--modal-interactive-transform-visible: scale(1, 1);
--modal-interactive-transform-invisible: scale(0, 0);

--modal-spacing-content-margin: 4em auto;
--modal-spacing-radius: 0;

PROPS
open: whether the modal is currently open or not, default false (mirrored as attribute)

*/


const template = document.createElement('template');
template.innerHTML = `
<style>
</style>
<div class="modal-veil" role="dialog">
	<div class="dialog modal-content">
		<div>
			<slot></slot>
			<div tabindex="-1" /><!-- for some reason, trap focus goes bonkers if this wc is the last component on the page -->
		</div>
	</div>
</div>
`;

export class VsModal extends HTMLElement {
	constructor() {
		super();

		this.setAttribute('role', 'dialog');

		this.attachShadow({ mode: 'open' });
		this.shadowRoot.appendChild(template.content.cloneNode(true));
		this.shadowRoot.querySelector('style').innerHTML = styles;
		this.content = this.shadowRoot.querySelector('.modal-content');
		this.veil = this.shadowRoot.querySelector('.modal-veil');
		this.updateVisibleState();

		this.content.addEventListener('keydown', this.closeViaEscape);
		this.veil.addEventListener('click', this.closeViaClick);
	}

	connectedCallback() {
		this.open = this.hasAttribute('open');
	}

	get tabbableElements() {
		if (this._tabbableElements) return this._tabbableElements;

		this._tabbableElements = setUpTabbableElements(this);

		return this._tabbableElements;
	}

	get open() {
		return this._open;
	}

	set open(value) {
		this._open = value;
		if (value) {
			this.setAttribute('open', String(value));
		} else {
			this.removeAttribute('open');
		}
		this.updateVisibleState();
	}

	updateFocus = (() => {
		if (this.open) {
			this._lastFocusedElement = document.activeElement;
			this.tabbableElements.forEach((element) => {
				element.setAttribute('tabindex', element.getAttribute('data-tabindex'));
			})
			this.tabbableElements[0].focus();
		} else {
			this._lastFocusedElement?.focus();
			this.tabbableElements.forEach((element) => {
				element.setAttribute('tabindex', '-1');
			})
		}
	}).bind(this);

	updateVisibleState = (() => {
		if (this.open) {
			this.classList.add('is-active');
			this.setAttribute('aria-expanded', 'true');
			this.setAttribute('aria-hidden', 'false');

			// trap-focus enabled, tabbing enabled, save currently focused element
		} else {
			this.classList.remove('is-active');
			this.setAttribute('aria-expanded', 'false');
			this.setAttribute('aria-hidden', 'true');
			// tabbing disabled, re-focus last-known element
		}
		this.updateFocus(this.tabbableElements);
	}).bind(this); 
	
	close = (() => {
		this.open = false;
		this.dispatchEvent(new Event('modal-closed'));
	}).bind(this);

	closeViaEscape = ((event) => {
		if (event.key === 'Escape') {
			this.close()
		}
	}).bind(this);

	closeViaClick = ((event) => {
		if (event.target === event.currentTarget) {
			this.close();
		}
	}).bind(this);

}

function getNumericTabindex(element) {
	const tabIndex = element.getAttribute('tabindex');
	if (isNaN(tabIndex)) return 0;

	return Number(tabIndex);
}

function setUpTabbableElements(parentNode) {
	const slottedTabbedElements = parentNode.querySelectorAll('a, button, input, select, textarea, [tabindex], vs-switch'); 

	const elementArray = Array.from(slottedTabbedElements).filter((element) => getNumericTabindex(element) >= 0);
	
	setUpFocusTrap(elementArray);

	return elementArray;
}

function setUpFocusTrap(elementArray) {
	if (elementArray.length === 0) return;
	
	const lastIndex = elementArray.length-1; 

	saveTabIndexes(elementArray);

	elementArray[0].addEventListener('keydown', (event) => {		
		if (event.key === 'Tab') {
			event.preventDefault();
			if (elementArray.length > 1) {
				if (event.shiftKey) {
					elementArray[lastIndex].focus();
				} else {
					elementArray[1].focus();
				}
			}
		}
	})

	elementArray[0].addEventListener('keyup', keyUpPreventDefault);
	elementArray[0].addEventListener('keydown', keyUpPreventDefault);

	if (lastIndex !== 0) {
		elementArray[lastIndex].addEventListener('keydown', (event) => {
			if (event.key === 'Tab') {
				event.preventDefault();
				if (event.shiftKey) {
					elementArray[lastIndex-1].focus();
				} else {
					elementArray[0].focus();
				}
			}
		})

		elementArray[lastIndex].addEventListener('keyup', keyUpPreventDefault);
		elementArray[lastIndex].addEventListener('keydown', keyUpPreventDefault);
	}

}

function saveTabIndexes(elementArray) {
	elementArray.forEach((element) => {
		const tabIndex = element.getAttribute('tabindex') || '0'
		element.setAttribute('data-tabindex', tabIndex);
	})
}

function keyUpPreventDefault(event) {
	if (event.key === 'Tab') {
		event.preventDefault();
	}
}

