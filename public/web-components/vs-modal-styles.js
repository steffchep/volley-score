"use strict";

export default /*css*/`

:host .modal-veil {
	display: block;
	position: fixed;
	inset: 0;
	line-height: 1.5em;
	transform: var(--modal-interactive-transform-invisible, scale(0, 0));
	transform-origin: var(--modal-interactive-transform-origin, top left);
	background-color: var(--modal-color-veil, rgba(0, 0, 0, 0.25));
	transition: transform var(--modal-interactive-transition-duration) var(--modal-interactive-transition-function, ease-in);
}
:host(.is-active) .modal-veil {
	transform: var(--modal-interactive-transform-visible, scale(1, 1));
}

.dialog {
	display: block;
	border: none;
}

.modal-content {
	padding: 2em 1em;
	border-radius: var(--modal-spacing-radius, 0);
	margin: var(--modal-spacing-content-margin, 4em auto);
	background: var(--modal-color-content-background, #fff);
	color: var(--modal-color-content-foreground);
	width: var(--modal-content-width, clamp(300px, 30vw, 1000px));
	max-height: var(--modal-content-max-height, 90dvh);
	overflow-y: auto;
}
`;