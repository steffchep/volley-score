"use strict";

import styles from './vs-settings-dialog-styles.js';
const template = document.createElement('template');
template.innerHTML = /* html */`
<style>
</style>
<vs-modal id="settings-modal">
	<section aria-labelledby="settings-heading" class="modal-content">
		<h2 id="settings-heading">Settings</h2>
		<fieldset class="fieldset">
			<legend>Color settings</legend>
			<vs-switch id="color-from-system" label="Follow system"></vs-switch>
			<vs-switch id="color-from-setting" label="Use dark mode"></vs-switch>
		</fieldset>
		<fieldset class="fieldset">
			<legend>Animation settings</legend>
			<vs-switch id="show-animations" label="Show animations"></vs-switch>
		</fieldset>
	</section>
</vs-modal>

`;

import { loadColorScheme, saveColorScheme, loadAnimationPref, saveAnimationPref, setUpColorSchemeSwitch, useSystemColorScheme, setAnimationPref } from '../script/index.js';
export class VsSettingsDialog extends HTMLElement {
	constructor() {
		super();
		const shadowRoot = this.attachShadow({ mode: 'open' });
		shadowRoot.appendChild(template.content.cloneNode(true));
		this.shadowRoot.querySelector('style').innerHTML = styles;

		this._modalElem = this.shadowRoot.getElementById('settings-modal');
		this._modalElem.style.setProperty('--modal-color-content-foreground', 'var(--vs-color-header-foreground)');
		this._modalElem.style.setProperty('--modal-color-content-background', 'var(--vs-color-header-background)');

		this._colorFromSystemElement = this.shadowRoot.querySelector('#color-from-system');
		this._colorFromSettingElement = this.shadowRoot.querySelector('#color-from-setting');
		this._showAnimationsElement = this.shadowRoot.querySelector('#show-animations');

		this._modalElem.addEventListener('dialog-opened', () => this.dispatchEvent(new Event('settings-opened')));
		this._modalElem.addEventListener('dialog-closed', () => this.dispatchEvent(new Event('settings-closed')));


		setTimeout(() => {
			const { setColorSchemeFromSystem, setColorSchemeFromSetting } = setUpColorSchemeSwitch(
					this._colorFromSettingElement, 
					loadColorScheme, saveColorScheme,
					() => this.colorScheme = loadColorScheme(useSystemColorScheme)
				);
		
			this._setColorSchemeFromSystem = setColorSchemeFromSystem;
			this._setColorSchemeFromSetting = setColorSchemeFromSetting;
	
			this._colorFromSystemElement.addEventListener('change', () => {
				this.useSystemSetting = this._colorFromSystemElement.checked;
			});

			this._showAnimationsElement.addEventListener('change', () => {
				this.animateCounters = this._showAnimationsElement.checked;
			});
	
		}, 0);
	}

	connectedCallback() {
		this.open = this.hasAttribute('open');
		const colorScheme = loadColorScheme(useSystemColorScheme);
		setTimeout(() => {
			// give child component time to initialize
			this.useSystemSetting = (colorScheme === 'system');
			this.colorScheme = colorScheme;
			this.animateCounters = loadAnimationPref();
		}, 0);
	}

	get animateCounters() {
		return this._showAnimationsElement.checked;
	}

	set animateCounters(value) {
		this._showAnimationsElement.checked = value;
		setAnimationPref(this.animateCounters);
		saveAnimationPref(value);
	}

	get colorScheme() {
		return this._colorScheme;
	}
	set colorScheme(value) {
		if (value === 'system' && !this._colorScheme) { 
			this._colorScheme = 'light'; 
			this._colorFromSettingElement.checked = false; 
			return;
		}
		if (!['dark', 'light'].includes(value)) return;
		this._colorScheme = value;
		this._colorFromSettingElement.checked = (value === 'dark'); 
		saveColorScheme(value);
	}

	set open(value) {
		if (value) {
			this.setAttribute('open', '');
		} else {
			this.removeAttribute('open');
		}
		if (value === this._modalElem.open) return;
		this._modalElem.open = value;
	}
	get open() {
		return this._modalElem.open;	
	}

	get useSystemSetting() {
		return this._colorFromSystemElement.checked;
	}
	set useSystemSetting(value) {
		this._colorFromSystemElement.checked = value;
		if (value) {
			saveColorScheme('system');
			this._colorFromSettingElement.disabled = true;
			this._setColorSchemeFromSystem();
		} else {
			this.colorScheme = this._colorScheme; 
			this._colorFromSettingElement.disabled = false;
			this._setColorSchemeFromSetting();
		}
	}
}
