"use strict";

// --modal-interactive-transform-visible-mobile: translateY(0);
// --modal-interactive-transform-invisible-mobile: translateY(105vh);
// --modal-interactive-margin-mobile: var(--vs-spacing-header-min-height, 0) auto auto auto;

export default /*css*/`

@import '/styles/styles.css';
:host {
	--modal-content-width: var(--vs-settings-dialog-width, clamp(300px, 85vw, 500px));
	--modal-interactive-transform-origin: top;
	--modal-interactive-transform-invisible: scale(1, 0);

	box-sizing: border-box;
	line-height: 1.5em;
	color: inherit;
	display: block;
	z-index: 10;
}
.fieldset {
	display: flex;
	flex-wrap: wrap;
	padding: var(--vs-spacing-4x);
	grid-template-columns: 1fr 1fr;
	gap: var(--vs-spacing-4x);
	border: 1px solid var(--vs-settings-border-color, rgba(255, 255, 255, 0.5));
}
.fieldset > * {
	flex-grow: 1;
	text-align: left;
	justify-content: start;
}
legend {
	padding-left: var(--vs-spacing-2x);
	padding-right: var(--vs-spacing-2x);
}
label {
	display: inline-grid;
	grid-template-columns: 3.5em 1fr;
	gap: var(--vs-spacing-2x);
}
`;