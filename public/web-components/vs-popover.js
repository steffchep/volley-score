"use strict";

import styles from './vs-popover-styles.js';

/*
CSS-VARIABLES (and defaults)
--modal-color-content-background: #fff;
--modal-color-content-foreground: initial;
--modal-color-veil: rgba(0, 0, 0, 0.25);

--modal-content-max-height: 90dvh;
--modal-content-width: clamp(300px, 80vw, 1000px);

--modal-interactive-transition-function: ease-in;
--modal-interactive-transition-duration: 500ms;
--modal-interactive-transform-origin: center;
--modal-interactive-transform-visible: scale(1, 1);
--modal-interactive-transform-invisible: scale(0, 0);

--modal-spacing-content-margin: 4em auto;
--modal-spacing-radius: 0;

PROPS
open: whether the modal is currently open or not, default false (mirrored as attribute)

*/


const template = document.createElement('template');
template.innerHTML = `
<style>
</style>
<dialog>
	<div class="modal-content">
		<slot></slot>
	</div>
</dialog>
`;

export class VsPopover extends HTMLElement {
	constructor() {
		super();

		this.attachShadow({ mode: 'open' });
		this.shadowRoot.appendChild(template.content.cloneNode(true));
		this.shadowRoot.querySelector('style').innerHTML = styles;
		this._dialogElement = this.shadowRoot.querySelector('dialog');


		// ESC will also close the modal by default, override it to get the animation
		this.addEventListener('keydown', (e) => {
			if (e.code === 'Escape') {
				e.preventDefault();
				this.open = false;
			}
		});

		this._dialogElement.addEventListener('click', (e) => {
			if (e.target === e.currentTarget) {
				e.stopPropagation();
				this.open = false;
			}
		});
	}

	connectedCallback() {
		this.open = this.hasAttribute('open');
	}

	get open() {
		return this._dialogElement.open;
	}

	set open(value) {
		if (value === this.open) { return; }
		if (value) {
			this.setAttribute('open', '');
			this._open();
		} else {
			this.removeAttribute('open');
			this._close();
		}
	}

	get modal() {
		return this.hasAttribute('modal') ;
	}

	set modal(value) {
		if (value) {
			this.setAttribute('modal', '');
		} else {
			this.removeAttribute('modal');
		}
	}
	
	async _open() {
		if (!this._dialogElement.open) {
			if (this.modal || window.innerWidth <= 510) {
				this._dialogElement.showModal();
			} else {
				this._dialogElement.show();
			}

			const {	easingFunction,	transitionDuration, transformVisible, transformInvisible } = getAnimationProperties(this);

			if (easingFunction && transitionDuration) {
				const animationOptions = { duration: transitionDuration, easing: easingFunction };
				const animationCssProps = {
					transform: [transformInvisible, transformVisible], 
					opacity: [0, 1], 
				};
				
				await this._dialogElement.animate( animationCssProps, animationOptions).finished;
			}
			this.dispatchEvent(new Event('dialog-opened', { bubbles: true, composed: true  }));
		} else {
			console.log('already open');
		}
	}
	
	async _close() {
		if (!this._dialogElement.open) { return; }

		const {	easingFunction,	transitionDuration, transformVisible, transformInvisible } = getAnimationProperties(this);
		
		if (easingFunction && transitionDuration) {
			const animationOptions = { duration: transitionDuration, easing: easingFunction };
			const animationCssProps = {
				transform: [transformVisible, transformInvisible], 
				opacity: [1, 0], 
			};
			await this._dialogElement.animate( animationCssProps, animationOptions).finished;
		}
		this._dialogElement.close();			
		this.dispatchEvent(new Event('dialog-closed', { bubbles: true, composed: true }));
	};

}

function getAnimationProperties(elem) {
	let easingFunction = getComputedStyle(elem).getPropertyValue('--modal-interactive-transition-function');
	if (easingFunction === 'none') {
		easingFunction = null;
	}
	return {
		easingFunction,
		transitionDuration: getMillisFromCssProp(getComputedStyle(elem).getPropertyValue('--modal-interactive-transition-duration')),
		transformVisible: getComputedStyle(elem).getPropertyValue('--modal-interactive-transform-visible'),
		transformInvisible: getComputedStyle(elem).getPropertyValue('--modal-interactive-transform-invisible'),
	}
}

export function getMillisFromCssProp(cssProp) {
	try {
		if (cssProp.includes('ms')) {
			return Number(cssProp.replace('ms', ''));
		} else if (cssProp.includes('s')) {
			const seconds = Number(cssProp.replace('s', ''));
			return seconds * 1000;
		} else {
			return 0;
		}

	} catch (error) {
		console.log({ error });
		return 500;
	}
}

