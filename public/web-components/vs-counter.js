"use strict";

import styles from './vs-counter-styles.js';
import { useSwipeEvents } from '../script/use-swipe-events.js';
const template = document.createElement('template');
template.innerHTML = `
<style>
</style>
<div class="counter-container">
	<button class="vs-counter-number">
		<div class="vs-counter-current" role="status"></div>
		<div class="vs-counter-slider">
			<div class="vs-counter-up"></div>
			<div class="vs-counter-down"></div>
		</div>
	</button>
	<button class="vs-counter-minus">
		<svg id="triangle-down" viewBox="0 0 100 100" preserveAspectRatio="none">
			<polygon points="50 0, 100 100, 0 100"/>
		</svg>
		<span class="is-overlaid">-</span>
	</button>
</div>
`;

export class VsCounter extends HTMLElement {
	constructor() {
		super();
		this.attachShadow({ mode: 'open' });
		this.shadowRoot.appendChild(template.content.cloneNode(true));
		this.shadowRoot.querySelector('style').innerHTML = styles;

		this.minusButton = this.shadowRoot.querySelector('.vs-counter-minus');
		this.counterButton = this.shadowRoot.querySelector('.vs-counter-number');
		this.counterCurrentElement = this.shadowRoot.querySelector('.vs-counter-current');
		this.counterSliderElement = this.shadowRoot.querySelector('.vs-counter-slider');
		this.counterUpElement = this.shadowRoot.querySelector('.vs-counter-up');
		this.counterDownElement = this.shadowRoot.querySelector('.vs-counter-down');

		this.value = 0;
		this._prevValue = 0;
		this._directionIndicator = 1;
		this.isFrozen = false;
		this.counterCurrentElement.textContent = this._value;

		this._setAriaLabels('');

		this.minusButton.addEventListener('click', this.minus);
		this.counterButton.addEventListener('click', this.plus);
		
		useSwipeEvents(this.counterButton);
		this.counterButton.addEventListener('swipe-up', this.plus);
		this.counterButton.addEventListener('swipe-down', this.minus);
	}

	get name() {
		return this.getAttribute('name');
	}

	set name(value) {
		this.setAttribute('name', value)
		this._setAriaLabels(value);
	}

	get isFrozen() {
		return this._isFrozen;
	}

	set isFrozen(value) {
		this._isFrozen = value;
		this.counterButton.disabled = value ? true : null;
	}

	set value(value) {
		if (value < this._value || value === 0) {
			this.isFrozen = false;
		}
		if (value >= 0 && value !== this._value) {
			this._prevValue = this._value ? this._value : Math.max(value - 1, 0);
			this._value = value;
			this.minusButton.disabled = null;
			this._animate();
			this.dispatchEvent(new CustomEvent('value-changed', { detail: this._prevValue }))
		}
		if (value <= 0) {
			this.minusButton.disabled = true;
		}
	}
	get value() {
		return this._value;
	}

	_plus() {
		this._directionIndicator = 1;
		this.value = this.value + 1;
	}
	plus = this._plus.bind(this);

	_minus() {
		this._directionIndicator = 0;
		this.value = this.value - 1;
	}
	minus = this._minus.bind(this);

	async _animate() {
		if (this._prevValue === this.value) { return; }
		if (!(window.matchMedia('(prefers-reduced-motion: no-preference)').matches)) { 
			this.counterCurrentElement.textContent = this.value;
			return; 
		};

		const transformEnd = `translateY(${this._directionIndicator * -50}%)`;
		const transformStart = `translateY(${flipDirection(this._directionIndicator) * -50}%)`;

		// put correct values in slider
		this.counterUpElement.textContent = this._directionIndicator ? this._prevValue : this.value;
		this.counterDownElement.textContent = this._directionIndicator ? this.value : this._prevValue;

		// align slider to show old value;
		this.counterSliderElement.style.transform = transformStart;
		// show the slider
		this.counterSliderElement.style.visibility = 'visible';
		// hide the static display
		this.counterCurrentElement.style.visibility = 'hidden';
		// slide to new value
		const props = getAnimationProperties(this);
		await this.counterSliderElement.animate(
			{ transform: [transformStart, transformEnd] },
			props,
		).finished;

		// replace value in static display
		this.counterCurrentElement.textContent = this.value;

		// hide slider, show static
		this.counterCurrentElement.style.visibility = 'visible';
		this.counterSliderElement.style.visibility = 'hidden';
	}

	_setAriaLabels(name) {
		this.minusButton.setAttribute('aria-label', `decrease score ${name}`);
		this.minusButton.setAttribute('title', `decrease score ${name}`);
		this.counterButton.setAttribute('aria-label', `increase score ${name}`);
		this.counterButton.setAttribute('title', `increase score ${name}`);
	}
}

function getAnimationProperties(elem) {
	let easingFunction = getComputedStyle(elem).getPropertyValue('--vs-interactive-transition-function'	);
	if (!easingFunction) {
		easingFunction = 'ease';
	}
	if (easingFunction === 'none') {
		easingFunction = null;
	}
	return {
		easing: easingFunction,
		duration: getMillisFromCssProp(getComputedStyle(elem).getPropertyValue('--vs-interactive-transition-duration')),
	};
}

function getMillisFromCssProp(cssProp) {
	try {
		if (cssProp.includes('ms')) {
			return Number(cssProp.replace('ms', ''));
		} else if (cssProp.includes('s')) {
			const seconds = Number(cssProp.replace('s', ''));
			return seconds * 1000;
		} else {
			return 0;
		}
	} catch (error) {
		console.log({ error });
		return 500;
	}
}

function flipDirection(number) {
	// 0 => 1 , 1 => 0
	return Math.abs(number - 1);
} 
