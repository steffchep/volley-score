"use strict";

import styles from './vs-counter-styles.js';
const template = document.createElement('template');
template.innerHTML = `
<style>
</style>
<button class="vs-counter-number">
</button>
<button class="vs-counter-minus">
	<svg id="triangle-down" viewBox="0 0 100 100" preserveAspectRatio="none">
		<polygon points="50 0, 100 100, 0 100"/>
	</svg>
	<span class="is-overlaid">-</span>
</button>
`;

export class VsCounter extends HTMLElement {
	constructor() {
		super();
		this.attachShadow({ mode: 'open' });
		this.shadowRoot.appendChild(template.content.cloneNode(true));
		this.shadowRoot.querySelector('style').innerHTML = styles;

		this.minusButton = this.shadowRoot.querySelector('.vs-counter-minus');
		this.valueElement = this.shadowRoot.querySelector('.vs-counter-number');

		this.value = 0;
		this.isFrozen = false;
		this.valueElement.textContent = this._value;

		this._setAriaLabels('');

		this.minusButton.addEventListener('click', this.minus);
		this.valueElement.addEventListener('click', this.plus);
	}

	get name() {
		return this.getAttribute('name');
	}

	set name(value) {
		this.setAttribute('name', value)
		this._setAriaLabels(value);
	}

	get isFrozen() {
		return this._isFrozen;
	}

	set isFrozen(value) {
		this._isFrozen = value;
		this.valueElement.disabled = value ? true : null;
	}

	set value(value) {
		if (value < this._value || value === 0) {
			this.isFrozen = false;
		}
		if (value >= 0 && value !== this._value) {
			const oldVal = this._value;
			this._value = value;
			this.valueElement.textContent = this._value;
			this.minusButton.disabled = null;
			this.dispatchEvent(new CustomEvent('value-changed', { detail: oldVal }))
		}
		if (value <= 0) {
			this.minusButton.disabled = true;
		}
	}
	get value() {
		return this._value;
	}

	_plus() {
		this.value = this.value + 1;
	}
	plus = this._plus.bind(this);

	_minus() {
		this.value = this.value - 1;
	}
	minus = this._minus.bind(this);

	_setAriaLabels(name) {
		this.minusButton.setAttribute('aria-label', `decrease score ${name}`);
		this.minusButton.setAttribute('title', `decrease score ${name}`);
		this.valueElement.setAttribute('aria-label', `increase score ${name}`);
		this.valueElement.setAttribute('title', `increase score ${name}`);
	}
}
