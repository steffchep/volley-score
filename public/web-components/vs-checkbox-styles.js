"use strict";

export default /*css*/`
	:host {
		box-sizing: border-box;
		display: inline-block;
		border-radius: var(--vs-spacing-radius, 99999px);
		border: 1px solid var(--vs-color-main-border);
	}
	:host(:not([disabled])) {
		cursor: pointer;
		box-shadow: var(--vs-interactive-shadow);
	}
	:host([disabled]) {
		opacity: 0.5;
	}
	:host(:not([disabled]):focus-within) {
		outline: 2px solid var(--vs-color-interactive-outline, initial);
		outline-offset: 2px;
		box-shadow: 0px 0px 0px 2px var(--vs-color-main-background);
	}
	:host([checked]) {
		background: var(--vs-color-interactive-outline);
		color: var(--vs-color-header-foreground);
	}
	label {
		cursor: inherit;
		position: relative;
		display: inline-flex;
		align-items: center;
		font-size: inherit;
		font-weight: bold;
		height: 1em;
		width: auto;
		padding: var(--vs-spacing-1x) var(--vs-spacing-2x);
	}
	input {
		cursor: inherit;
		position: absolute;
		opacity: 0;
	}
	input:focus {
		outline: none;
	}
`;
