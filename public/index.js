"use strict";

import { registerServiceWorker } from './service-worker-registration.js';

import { 
	VsCheckbox,
	VsCounter, 
	VsModal, 
	VsDialog, 
	// VsPopover,
	VsSettingsDialog, 
	VsStartGameDialog, 
	VsSwitch,
	VsTimeouts,
 } from './web-components/index.js';
import { addHandlers } from './script/index.js';

if (typeof HTMLDialogElement === 'function') {
	customElements.define('vs-modal', VsDialog);
} else {
	console.log('using LEGACY modal div');
	customElements.define('vs-modal', VsModal);
}

window.addEventListener('load', registerServiceWorker);

customElements.define('vs-checkbox', VsCheckbox);
customElements.define('vs-counter', VsCounter);
customElements.define('vs-start-game-dialog', VsStartGameDialog);
customElements.define('vs-settings-dialog', VsSettingsDialog);
customElements.define('vs-switch', VsSwitch);
customElements.define('vs-timeouts', VsTimeouts);

const elements = {
	clearLogButton: document.getElementById('clear-log'),
	confirmationDialog: document.getElementById('confirmation-dialog'),
	copyButton: document.getElementById('copy-to-clipboard'),
	endGameButton: document.getElementById('end-game'),
	logOutput: document.getElementById('log-output'),
	nextSetButton: document.getElementById('next-set'),
	resetScoreButton: document.getElementById('reset-score'),
	settingsButton:  document.getElementById('change-settings'),
	settingsDialog: document.getElementById('settings-dialog'),
	sides: document.querySelectorAll('.court-side'),
	startGameButton: document.getElementById('start-game'),
	startGameDialog: document.querySelector('vs-start-game-dialog'),
	switchSidesButton: document.getElementById('switch-sides'),
}

addHandlers(elements);

console.log('initialized');
